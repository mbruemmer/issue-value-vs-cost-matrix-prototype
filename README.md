# issue value vs cost matrix prototype

Small prototype for https://gitlab.com/gitlab-org/gitlab/issues/202041 using plotly

# Usage

* pip install plotly
* jupyter notebook
* run each cell

# Issues

Issues were crawled on 2020-01-31, sorted by upvotes and top 1000 non confidential were used.